import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { UsersModule } from './../src/models/users/users.module';
import { AuthModule } from './../src/auth/auth.module';

describe('UsersController (e2e)', () => {
  let app: INestApplication;
  const adminUser: string = 'admin';
  const adminPassword: string = 'changeme';

  const normalUser: string = 'user';
  const normalPassword: string = 'guess';

  const requestAdmin = async () => {
    return await request(app.getHttpServer())
      .post('/auth/login')
      .send({ username: adminUser, password: adminPassword });
  };

  const requestUser = async () => {
    return await request(app.getHttpServer())
      .post('/auth/login')
      .send({ username: normalUser, password: normalPassword });
  };

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [UsersModule, AuthModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('(GET) Users - 401', async () => {
    const result = await request(app.getHttpServer()).get('/users/');
    expect(result.status).toBe(401);
  });

  it('(GET) Use auth but wrong role - 403', async () => {
    const authData = await requestUser();

    return request(app.getHttpServer())
      .get('/users/')
      .set('authorization', `Bearer ${authData.body.access_token}`)
      .expect(403);
  });

  it('(GET) all user - 200', async () => {
    const authData = await requestAdmin();

    const result = await request(app.getHttpServer())
      .get('/users/')
      .set('authorization', `Bearer ${authData.body.access_token}`);

    expect(result.status).toBe(200);
    expect(result.body.length).toBe(2);
  });

  it('(GET) get user by specified text - 200', async () => {
    const authData = await requestAdmin();

    const result = await request(app.getHttpServer())
      .get('/users?q=admin')
      .set('authorization', `Bearer ${authData.body.access_token}`);

    expect(result.status).toBe(200);
    expect(result.body.length).toBe(1);
  });

  it('(POST) create user ', async () => {
    const authData = await requestAdmin();
    const result = await request(app.getHttpServer())
      .post('/users')
      .set('authorization', `Bearer ${authData.body.access_token}`)
      .send({ givenName: 'ton', email: 'ton@gmail.com' });

    expect(result.status).toBe(201);
    expect(typeof result.body.userId === 'string').toBeTruthy();
  });

  it('(PUT) update user', async () => {
    const authData = await requestAdmin();
    const createdUser = await request(app.getHttpServer())
      .post('/users')
      .set('authorization', `Bearer ${authData.body.access_token}`)
      .send({ givenName: 'ton', email: 'ton@gmail.com' });

    const updatedUser = await request(app.getHttpServer())
      .put('/users/' + createdUser.body.userId)
      .set('authorization', `Bearer ${authData.body.access_token}`)
      .send({ givenName: 'ton_edited', email: 'ton_edited@gmail.com' });

    expect(updatedUser.status).toBe(200);
    expect(updatedUser.body.id).toBe(createdUser.body.userId);
    expect(updatedUser.body.givenName).toBe('ton_edited');
    expect(updatedUser.body.email).toBe('ton_edited@gmail.com');
  });

  it('(Get) get user by id', async () => {
    const authData = await requestAdmin();
    const createdUser = await request(app.getHttpServer())
      .post('/users')
      .set('authorization', `Bearer ${authData.body.access_token}`)
      .send({ givenName: 'ton', email: 'ton@gmail.com' });

    const user = await request(app.getHttpServer())
      .get('/users/' + createdUser.body.userId)
      .set('authorization', `Bearer ${authData.body.access_token}`);

    expect(user.status).toBe(200);
    expect(user.body.givenName).toBe('ton');
    expect(user.body.email).toBe('ton@gmail.com');
    expect(user.body.id).toBe(createdUser.body.userId);
  });

  it('(Delete) delete user', async () => {
    const authData = await requestAdmin();
    const userList = await request(app.getHttpServer())
      .get('/users/')
      .set('authorization', `Bearer ${authData.body.access_token}`);

    const user = userList.body[1];
    const deleteRes = await request(app.getHttpServer())
      .delete('/users/' + user.id)
      .set('authorization', `Bearer ${authData.body.access_token}`);

    expect(deleteRes.status).toBe(200);
    expect(deleteRes.text).toBe('OK');

    const userListAfterDel = await request(app.getHttpServer())
      .get('/users/')
      .set('authorization', `Bearer ${authData.body.access_token}`);

    expect(userListAfterDel.body.length).toBe(1);
  });
});
