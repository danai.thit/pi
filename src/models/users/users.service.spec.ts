import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from './users.service';
import { v4 } from 'uuid';

describe('UsersService', () => {
  let service: UsersService;
  let testID: string | null;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UsersService],
    }).compile();

    service = module.get<UsersService>(UsersService);
    testID = await service.create('tester', 'tester@gmail.com');
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should be create user', async () => {
    const id = await service.create('Ton', 'ton@gmail.com');
    const user = await service.findById(id);

    expect(user.email).toEqual('ton@gmail.com');
    expect(user.givenName).toEqual('Ton');
    expect(user.id).toBe(id);
  });

  it('should be error whiling create user duplicate', async () => {
    try {
      await service.create('Ton', 'ton@gmail.com');
      async () => await service.create('Ton', 'ton@gmail.com');
    } catch (e) {
      expect(e).toEqual(new Error('Something bad happened'));
    }
  });

  it('should be find all user', async () => {
    await service.create('Ton', 'ton@gmail.com');
    await service.create('Ton 2', 'ton2@gmail.com');
    const users = await service.findAll();
    expect(users.length).toEqual(5);
  });

  it('should be find all by query', async () => {
    await service.create('Ton', 'ton@gmail.com');
    await service.create('Ton 2', 'ton2@gmail.com');
    await service.create('Tan 2', 'ton3@gmail.com');
    const users = await service.findAll('on');
    expect(users.length).toEqual(3);
    expect(users.every((e) => e.email.includes('on'))).toBeTruthy();
    expect(users.filter((e) => e.givenName.includes('on')).length).toBe(2);
    expect(users.filter((e) => e.givenName.includes('an')).length).toBe(1);
  });

  it('should be find by id - found', async () => {
    const user = await service.findById(testID);
    expect(user.email).toEqual('tester@gmail.com');
    expect(user.givenName).toEqual('tester');
  });

  it('should be find by id - not found', async () => {
    const id = v4();
    const user = await service.findById(id);
    expect(user).toEqual(undefined);
  });

  it('should be update user', async () => {
    const user = await service.findById(testID);

    user.email = 'danai.thit@gmail.com';
    user.givenName = 'danai';
    const newData = await service.update(user);

    expect(newData.email).toEqual('danai.thit@gmail.com');
    expect(newData.givenName).toEqual('danai');
  });
});
