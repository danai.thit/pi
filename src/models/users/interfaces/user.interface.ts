import { Role } from '../../../common/enums/role.enum';

interface User {
  id: string;
  username?: string;
  givenName: string;
  email: string;
  password?: string;
  roles?: Role[];
}

export default User;
