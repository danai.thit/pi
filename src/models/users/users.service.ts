import { Injectable } from '@nestjs/common';
import User from './interfaces/user.interface';
import { v4 } from 'uuid';
import { Role } from '../../common/enums/role.enum';

@Injectable()
export class UsersService {
  private users: User[] = [
    {
      id: v4(),
      username: 'admin',
      givenName: 'admin',
      email: 'admin@gmail.com',
      password: 'changeme',
      roles: [Role.Admin],
    },
    {
      id: v4(),
      username: 'user',
      givenName: 'user',
      email: 'user@gmail.com',
      password: 'guess',
      roles: [Role.User],
    },
  ];

  async create(givenName: string, email: string): Promise<string> {
    try {
      const inDb = this.users.find(
        (e) => e.givenName === givenName || e.email === email,
      );

      if (inDb) {
        throw new Error('Something bad happened');
      }
      const id = v4();
      await this.users.push({ id: id, givenName, email });
      return id;
    } catch (error) {
      throw error;
    }
  }

  async update(user: User): Promise<User> {
    const targetUser = this.users.find((e) => e.id === user.id);

    if (targetUser) {
      targetUser.givenName = user.givenName;
      targetUser.email = user.email;
      return targetUser;
    } else {
      throw new Error('Something bad happened');
    }
  }

  async delete(id: string): Promise<void> {
    const targetUser = this.users.find((e) => e.id === id);
    if (targetUser) {
      this.users = this.users.filter((e) => e.id !== id);
    } else {
      throw new Error('Something bad happened');
    }
  }

  async findAll(query: null | string = null): Promise<User[]> {
    if (query)
      return await this.users.filter(
        (e) => e.email.includes(query) || e.givenName.includes(query),
      );
    return await this.users;
  }

  async findById(id: string): Promise<User | undefined> {
    return this.users.find((e) => e.id === id);
  }

  async findByUsername(username: string): Promise<User | undefined> {
    return this.users.find((user) => user.username === username);
  }
}
