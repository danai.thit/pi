import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  InternalServerErrorException,
  Query,
  Body,
  Param,
  BadRequestException,
  HttpCode,
} from '@nestjs/common';
import { UsersService } from './users.service';
import User from './interfaces/user.interface';
import UserDTO from './dto/user.dto';
import { Role } from '../../common/enums/role.enum';
import { Roles } from '../../role/role.decorator';

@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Get('/')
  @Roles(Role.Admin)
  async getUsers(@Query('q') query: string = ''): Promise<User[]> {
    try {
      const users = await this.usersService.findAll(query);
      return users;
    } catch (e) {
      throw new InternalServerErrorException();
    }
  }

  @Post('/')
  @HttpCode(201)
  @Roles(Role.Admin)
  async createUser(@Body() userDto: UserDTO) {
    try {
      const userId = await this.usersService.create(
        userDto.givenName,
        userDto.email,
      );

      return { userId: userId };
    } catch (e) {
      throw new InternalServerErrorException();
    }
  }

  @Get('/:id')
  @Roles(Role.Admin)
  async getUserById(@Param('id') id: string): Promise<User> {
    try {
      const user = await this.usersService.findById(id);
      return user;
    } catch (e) {
      throw new InternalServerErrorException();
    }
  }

  @Put('/:id')
  @Roles(Role.Admin)
  async updateUser(
    @Param('id') id: string,
    @Body() userDto: UserDTO,
  ): Promise<User> {
    try {
      if (id) {
        const user: User = {
          id: id,
          givenName: userDto.givenName,
          email: userDto.email,
        };

        const result = await this.usersService.update(user);
        return result;
      } else {
        throw new BadRequestException();
      }
    } catch (e) {
      throw new InternalServerErrorException();
    }
  }

  //@patch is uesd for update some property/field

  @Delete('/:id')
  @Roles(Role.Admin)
  async deleteUser(@Param('id') id: string): Promise<string> {
    try {
      await this.usersService.delete(id);
      return 'OK';
    } catch (e) {
      throw new InternalServerErrorException();
    }
  }
}
